"""Driver for AHT2x sensors (humidity and temperature):
    Models: AHT20 and AHT21
    Adresse i2c: 56 (0x3b)
    Official website of the manufacturer: http://www.aosong.com
"""

import time
from micropython import const

__author__ = "Jonathan Fromentin"
__credits__ = ["Jonathan Fromentin"]
__license__ = "CeCILL version 2.1"
__version__ = "0.1.0"
__maintainer__ = "Jonathan Fromentin"


AHT_I2C_ADDR = const(0x38)  # Default I2C address
AHT_STATUS_BUSY = const(0x01)  # Status bit for busy
AHT_STATUS_CALIBRATED = const(0x10) # status bit for calibrated
AHT_CMD_INIT = const(0xbe) # command for initialization
AHT_CMD_TRIGGER = const(0xac) # command for trigger measurement
AHT_CMD_RESET = const(0xba) # command for soft reset
AHT_CRC_POLYNOMIAL = const(0x31) # Polynomial representation
AHT_CRC_MSB = const(0x80) # Most significant bit
AHT_CRC_INIT = const(0xff) #Initial value of CRC

class AHT2x:
    """Class based on AHT20 and AHT21 documentation."""

    def __init__(self, i2c, address=AHT_I2C_ADDR, crc=False):
        """Parameters:
            i2c: instance of machine.I2C
            address: i2c address of sensor
            crc: Boolean for CRC control True to active the CRC control."""
        time.sleep(0.04)  # Wait  40ms  after  power-on.
        self.i2c = i2c
        self.address = address
        self.active_crc = crc
        self.valid_crc = False
        self._buf = bytearray(7)
        self._temp = None
        self._humidity = None
        while not self.status & AHT_STATUS_CALIBRATED:
            self._calibrate()

    @property
    def status(self):
        """The status byte initially returned from the sensor.
        Bit     Definition  Description
        [0:2]   Remained    Remained
        [3]     CAL Enable  0:Uncalibrated,1:Calibrated
        [4]     Remained    Remained
        [5:6]   Remained    Remained
        [7]     Busy        0:Free in dormant state, 1:Busy in measurement"""
        self.i2c.readfrom_into(self.address, self._buf)
        return self._buf[0]

    @property
    def humidity(self):
        """The measured relative humidity in percent."""
        return self._measure()[0]

    @property
    def temperature(self):
        """The measured temperature in degrees Celsius."""
        return self._measure()[1]

    @property
    def measures(self):
        """The measured relavite humidity and temperature. This is the optimal
        method when you want both types of measurements at the same time."""
        return self._measure()

    def reset(self):
        """This command is used to restart the sensor system without turning
        the power off and on again. After receiving this command, the sensor
        system begins to re-initialize and restore the default setting state"""
        self._buf[0] = AHT_CMD_RESET
        self.i2c.writeto(self.address, self._buf[0])
        time.sleep(0.02) # The time required for reset does not exceed 20 ms

    def _calibrate(self):
        """Internal function to send initialization command.
        Note: The  calibration  status  check  in  the  first  step
            only  needs  to  be  checked  at  power-on.  No  operation is
            required during the normal acquisition process."""
        self._buf[0] = AHT_CMD_INIT
        self._buf[1] = 0x08
        self._buf[2] = 0x00
        self.i2c.writeto(self.address, self._buf[:3])
        time.sleep(0.01) # Wait initialization process

    def _crc8(self):
        """Internal function to calcule the CRC-8-Dallas/Maxim of current
        message. The initial value of CRC is 0xFF, and the CRC8 check
        polynomial is: CRC [7:0] = 1+X^4 +X^5 +X^8"""
        crc = bytearray(1)
        crc[0] = AHT_CRC_INIT
        for byte in self._buf[:6]:
            crc[0] ^= byte
            for _ in range(8):
                if crc[0] & AHT_CRC_MSB:
                    crc[0] = (crc[0] << 1) ^ AHT_CRC_POLYNOMIAL
                else: crc[0] = (crc[0] << 1)
        self.val_crc = crc[0]
        return crc[0]

    def _measure(self):
        """Internal function for triggering the AHT to read temp/humidity"""
        self._buf[0] = 0xac
        self._buf[1] = 0x33
        self._buf[2] = 0x00
        self.i2c.writeto(self.address, self._buf[:3])
        time.sleep(0.08) #Wait 80ms for the measurement to be completed.
        while self.status & AHT_STATUS_BUSY:
            time.sleep(0.01)
        self.i2c.readfrom_into(self.address, self._buf)

        if self.active_crc:
            self.valid_crc = self._crc8() == self._buf[6]

        if not self.active_crc or self.valid_crc:
            humidity = (
                (self._buf[1] << 12)
                | (self._buf[2] << 4)
                | (self._buf[3] >> 4))
            humidity = (humidity * 100) / 0x100000
            temperature = (
                ((self._buf[3] & 0xF) << 16)
                | (self._buf[4] << 8)
                | self._buf[5])
            temperature = ((temperature * 200.0) / 0x100000) - 50
        else:
            humidity = 0
            temperature = 0
        return (humidity, temperature)
